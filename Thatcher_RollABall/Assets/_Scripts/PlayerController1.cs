﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement; //Let's you use scene mananagement code in this script

public class PlayerController1 : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;

    Rigidbody rb;

    private int count;

    bool onGround = true;// Whether the player is on the ground, if so let them jump
    public float jumpPower;// How high the player jumps, set in the editor


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    void Update()
    {
        //Shoot a line below the player that extends .52f downwards
        //if it hits something true (we're on the ground)
        //if not return false (we're in the air)
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);

        if (Input.GetButtonDown("Jump") && onGround) //if pressing jump button and in air
        {
            Jump();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            speed = speed * 4f;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            speed = speed / 4f;
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            GetComponent<Renderer>().material.color = Color.red;
        }
        if(Input.GetKeyDown(KeyCode.G))
        {
            GetComponent<Renderer>().material.color = Color.green;
        }
        if(Input.GetKeyDown(KeyCode.B))
        {
            GetComponent<Renderer>().material.color = Color.blue;
        }

    }

    void Jump()
    {
        //Add force based on jump power upwards to jump
        rb.AddForce(Vector3.up * jumpPower);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }
    void SetCountText()
    {
        countText.text = "Count:" + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win";

            Invoke("RestartLevel", 2f);//Call restart after 2 seconds
        }

    }
    void RestartLevel()
    {
        //Reload the current level 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

